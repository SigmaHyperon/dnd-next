FROM node:alpine

WORKDIR /
COPY package.json package.json
COPY tsconfig.json tsconfig.json
COPY tslint.json tslint.json
COPY src src
COPY config config
RUN npm i
RUN npm run-script build
CMD [ "node", "." ]