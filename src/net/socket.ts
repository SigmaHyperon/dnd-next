import socketio from "socket.io";
import http from "./web";
import Log, { component } from "../lib/log/log";

const io = socketio(http);

io.on("connection", (socket: socketio.Socket) => {
	Log.log("client connected", component.socket, socket.handshake.address);
});

export = io;