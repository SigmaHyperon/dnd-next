import express from "express";
import Config from "../lib/Config";
import Log, { component } from "../lib/log/log";

const app = express();
const port:number = Config.default("web/port", 3000);

var http = require("http").Server(app);

app.get("/", (req: any, res: any) => {
  res.send("hello world");
});

const server = http.listen(port, function() {
  Log.log("server listening on port", component.http, port);
});

export = http;