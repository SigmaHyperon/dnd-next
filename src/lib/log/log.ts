import winston from "winston";

export enum logLevel {
	error   = "error",
	warning = "warn",
	info    = "info",
	http	= "http",
	verbose = "verbose",
	debug   = "debug",
	silly	= "silly"
}

export enum component {
	socket = "socket",
	http = "http",
	core = "core"
}

export default class Log {
	
	protected static getLogFormat(): any {
		return winston.format.combine(
			winston.format.label({ label: '[my-label]' }),
			winston.format.timestamp({
				format: 'YYYY-MM-DD HH:mm:ss'
			}),
			winston.format.printf(info => {
				return `${info.timestamp} ${info.level}: ${info.message}`;
			})
		);
	}
	
	protected static getLogTransports(): any[] {
		return [
			new winston.transports.Console(),
			// new winston.transports.File({ filename: 'combined.log' })
		];
	}
	protected static instance: winston.Logger = winston.createLogger({
		format: Log.getLogFormat(),
		transports: Log.getLogTransports()
	});

	private static log_(level: logLevel, message: string, comp?: component, ...rest: any[]): void {
		this.instance.log(level, `[${typeof comp === "undefined" ? component.core : comp}] ${message}`, ...rest);
	}

	public static  log(message: string, comp?: component, ...rest: any[]): void {
		this.log_(logLevel.info, message, comp, ...rest);
	}
	public static error(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.error, message, comp, ...rest);
	}
	public static warning(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.warning, message, comp, ...rest);
	}
	public static info(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.info, message, comp, ...rest);
	}
	public static http(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.http, message, comp, ...rest);
	}
	public static verbose(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.verbose, message, comp, ...rest);
	}
	public static debug(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.debug, message, comp, ...rest);
	}
	public static silly(message: string, comp?: component, ...rest: any[]){
		this.log_(logLevel.silly, message, comp, ...rest);
	}
}