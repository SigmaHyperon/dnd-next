import config from "config";

function def<T>(source: T, defaultValue: T) {
	if(typeof source === typeof undefined) {
		return defaultValue;
	}
	return source;
}

export default class Config {
	public static has = config.has;
	public static get = config.get;
	public static default<T>(configString: string, defaultValue: T): T {
		return def(this.get(configString), defaultValue);
	}
}


